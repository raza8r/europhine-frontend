import Head from "next/head";
import { useRouter } from "next/router";
import ProductGrid from "../Sections/ProductGrid";

export default function Products() {
  const router = useRouter();
  const path = router.asPath.split("/");
  const id = path[2]
  console.log("Category id", id);
  debugger;
  return (
    <div>
      <Head>
        <title>Products</title>
        <meta
          name="description"
          content="Generated by create next app"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <ProductGrid />
    </div>
  );
}
